<?php

/**
 * generates a hash to be used in the confirmation email
 * @param type $lenth
 * @return string
 */
function _generate_hash() {
  $lenth = 15;
  $model = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $hash = '';
  for ($i = 0; $i < $lenth; $i++) {
    $hash .= $model[rand(0, strlen($model) - 1)];
  }
  return $hash;
}

/**
 * mail headers to be used in the mail functions
 * @return string
 */
function _mail_headers() {
  $send_mail = _email_addr();
  //$from = _send_from();
  $headers = "From: " . $send_mail . "\r\n";
  $headers .= "Reply-To: " . $send_mail . "\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=UTF-8" . "\r\n";
  return $headers;
}

/**
 * mail footer to be used in the mail functions
 * @return string
 */
function _mail_footer($mail) {
  $hash = db_select('easysignup_users', 'es')
      ->fields('es', array('hash'))
      ->condition('email', $mail)
      ->condition('confirmed', 1, '=')
      ->execute()
      ->fetchField();
  $text = '';
  if ($hash !== FALSE) {
    global $base_url;
    $manage = $base_url . '/manage?c=' . $hash;
    $unsubsribe = $base_url . '/unsubsribe?c=' . $hash;
    $text .= '<p style="padding-top:200px;"><b>' . t('Subscription Management') . '</b><br/>';
    $text .= t('Use the following links, to customize your subscription or to unsubscribe from this') . ':<br/><br/>';
    $text .= t('!manage', array('!manage' => l(t('Customize subscription'), $manage))) . '<br/>'; //new_string
    $text .= t('!unsubscribe', array('!unsubscribe' => l(t('Unsubscribe'), $unsubsribe))) . '<br/>'; //new_string
    $text.='</p>';
  }
  $text .= "</body></html>";
  return $text;
}

/**
 * returns the sender name
 * @return string
 */
function _send_from() {
  $stored_name = variable_get('easysignup_admin', '');
  $site_name = variable_get('site_name', '');
  $sent_from = ($stored_name !== '') ? $stored_name : $site_name;
  return $sent_from;
}

/**
 * returns the sender email
 * @return string
 */
function _email_addr() {
  $stored_mail = variable_get('easysignup_mail', '');
  $site_mail = variable_get('site_mail', '');
  $email_addr = ($stored_mail !== '') ? $stored_mail : $site_mail;
  return $email_addr;
}

/**
 * returns the list of content types
 * @return array
 */
function _get_content_types() {
  $output = array();
  $types = node_type_get_types();
  foreach ($types as $type => $obj) {
    $output[$type] = $obj->name;
  }
  return $output;
}

/**
 * check if the submitted email is used
 * @param string $mail
 * @return string or fale
 */
function _subscription_verif($mail) {
  $query = db_select('easysignup_users', 'es')
      ->fields('es', array('email'))
      ->condition('email', $mail)
      ->execute()
      ->fetchField();
  return $query;
}

/**
 * record the user data in the easysignup_users table
 * @param string $mail
 * @param string $hash
 */
function _record_user($mail, $hash, $bundles) {
  $fields = array(
    'email' => $mail,
    'bundles' => $bundles,
    'hash' => $hash,
    'confirmed' => 0,
  );
  db_insert('easysignup_users')->fields($fields)->execute();
}

/**
 * send a confirmation mail to the subscription
 * @param string $to
 * @param string $link
 */
function _send_confirmation_mail($to, $link) {
  $subject = t('Subscription to new content from ') . ' ' . _send_from();
  $headers = _mail_headers();
  $message = '<html><body>';
  $message .= '<p>' . t('Complete registration process.<br/>Please confirm your email by clicking below') . '</p>';
  $message .= '<p>' . $link . '</p>';
  $message .= _mail_footer($to);
  mail($to, $subject, $message, $headers);
}

/**
 * verify if the confirmation hash is valid, then set user to confirmed
 * redirect him to front page and set a confirmation message
 */
function _confirmation() {
  $link = drupal_get_query_parameters();
  if (isset($link['c'])) {
    $result = db_select('easysignup_users', 'es')
        ->fields('es', array('hash'))
        ->condition('hash', $link['c'])
        ->condition('confirmed', 0, '=')
        ->execute()
        ->fetchField();
    //if hash is valid
    if ($link['c'] == $result) {
      $site_frontpage = variable_get('site_frontpage', 'node');
      db_update('easysignup_users')
          ->fields(array('confirmed' => 1))
          ->condition('hash', $result)
          ->execute();
      drupal_set_message(t('You will receive a notification email, once new content is published in your chosen categories.'));
      drupal_goto($site_frontpage);
    }
  }//dpm(get_defined_vars());
}

/**
 * calls a form to manage the subscription
 * @return string form to manage the subscription
 */
function _manage() {
  $link = drupal_get_query_parameters();
  if (isset($link['c'])) {
    $hash_var = db_select('easysignup_users', 'es')
        ->fields('es', array('hash'))
        ->condition('hash', $link['c'])
        ->execute()
        ->fetchField();
    if (!empty($hash_var)) {
      return drupal_get_form('_set_manage_form');
    }
  }
}

/**
 * returnsa form to manage the subscription
 * @param type $form
 * @param type $form_state
 * @return string
 */
function _set_manage_form($form, &$form_state) {
  $link = drupal_get_query_parameters();
  $query = db_select('easysignup_users', 'es')
      ->fields('es', array('bundles'))
      ->condition('hash', $link['c'])
      ->execute()
      ->fetchField();
  $default = unserialize($query);
  $active = variable_get('easysignup_active', array());
  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#description' => t('All available content types.'),
    '#options' => drupal_map_assoc($active),
    '#default_value' => drupal_map_assoc($default)
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
    '#value' => t('Submit'),
  );
  $form['#submit'][] = '_set_manage_form_submit';
  //dpm(get_defined_vars());
  return $form;
}

/**
 * callback function called when submitting the _set_manage_form
 * @param type $form
 * @param type $form_state
 */
function _set_manage_form_submit($form, &$form_state) {
  $serialized = array();
  $new_hash = _generate_hash();
  $c_types = $form_state['values']['content_types'];
  foreach ($c_types as $value) {
    if ($value !== 0) {
      $serialized[] = $value;
    }
  }
  $link = drupal_get_query_parameters();
  $site_frontpage = variable_get('site_frontpage', 'node');
  $hash_var = db_select('easysignup_users', 'es')
      ->fields('es', array('hash'))
      ->condition('hash', $link['c'])
      ->execute()
      ->fetchField();

  db_update('easysignup_users')
      ->fields(
          array(
            'bundles' => serialize($serialized),
            'hash' => $new_hash
      ))
      ->condition('hash', $hash_var)
      ->execute();
  drupal_set_message(t('Your choice has been saved'));
  drupal_goto($site_frontpage);
}

/**
 * callback function to unsubscribe a user
 */
function _unsubsribe() {
  $site_frontpage = variable_get('site_frontpage', 'node');
  $link = drupal_get_query_parameters();
  if (isset($link['c'])) {
    $hash_var = db_select('easysignup_users', 'es')
        ->fields('es', array('hash'))
        ->condition('hash', $link['c'])
        ->execute()
        ->fetchField();

    if (!empty($hash_var)) {
      db_delete('easysignup_users')->condition('hash', $hash_var)->execute();
      drupal_set_message(t('You are successfully unsubscribed.'));
      drupal_goto($site_frontpage);
    }
  }
}

/**
 * stores node default status in the "easysignup status" table
 * @param int $nid
 * @return int $status
 */
function _node_status($nid) {
  $status = 0;
  $fetch = db_select('easysignup_nodes', 'es')
      ->fields('es', array('nid'))
      ->condition('nid', $nid)
      ->execute()
      ->fetchField();
  if ($fetch !== FALSE) {
    $status = 1;
  }//dpm(get_defined_vars());
  return $status;
}

/**
 * generates a table of all subscribers
 * @return string
 */
function _subscribers_list() {
  $recipient = array();
  $ml = db_select('easysignup_users', 'es')
      ->fields('es', array('email', 'bundles'))
      ->condition('confirmed', 1)
      ->execute();
  while ($fetch = $ml->fetchAssoc()) {
    if (isset($fetch)) {
      $recipient[$fetch['email']] = unserialize($fetch['bundles']);
    }
  }
  $out = '<table>';
  $out .= '<thead><tr><td>' . t('Email') . '</td><td>' . t('Node types') . '</td></tr></thead>';
  foreach ($recipient as $key => $value) {
    $out .= '<tr><td>' . $key . '</td><td>' . implode(' ,', $value) . '</td></tr>';
  }
  $out .= '<table>';
  return $out;
}

/**
 * send mass mails
 * @global string $base_url
 * @param array $scope
 */
function _send_mass($scope) {
  global $base_url;
  $headers = _mail_headers();
  $subject = t('Latest news from') . ' "' . _send_from() . '"';
  foreach ($scope as $addr => $v) {
    $message = '<html><body style="font-family:arial;">';
    $message .= '<p>' . t('Below you will find an overview of new and updated content for your subscribed areas:') . '</p>';
    foreach ($v as $type => $nid) {
      $message .= $type . "\n";
      $message .= '<ul>';
      for ($n = 0; $n < count($nid); $n++) {
        $node = node_load($nid[$n]);
        $message .= '<li>' . l($node->title, $base_url . '/node/' . $node->nid, array('external' => TRUE)) . '</li>';
      }
      $message .= '</ul>';
    }
    $message .= _mail_footer($addr);
    mail($addr, $subject, $message, $headers);
    $message = '';
  }

  foreach ($scope as $b) {
    foreach ($b as $ch) {
      db_update('easysignup_nodes')
          ->fields(array('in_pool' => 0))
          ->condition('nid', $ch)
          ->execute();
    }
  }
  //dpm(get_defined_vars());
}

/**
 * build a big array for the mailing list
 * @return array
 */
function _mailing_list() {
  //array of the mail and his attached node_types
  $recipient = array();
  $get_u = db_select('easysignup_users', 'es')
      ->fields('es', array('email', 'bundles'))
      ->condition('confirmed', 1, '=')
      ->execute()
      ->fetchAll();
  foreach ($get_u as $u) {
    $recipient[$u->email] = unserialize($u->bundles);
  }

  $pool = _get_pool();
  foreach ($pool as $p_row => $p_line) {
    $n = db_select('node_type', 'nt')
        ->fields('nt', array('name'))
        ->condition('type', $p_line)
        ->execute()
        ->fetchField();
    $pool[$p_row] = $n;
  }
  $tree = array();
  foreach ($recipient as $rk => $rv) {
    foreach ($pool as $pk => $pv) {
      if (in_array($pv, $rv)) {
        $tree[$rk][$pv][] = $pk;
      }
    }
  }
  $node_types = db_select('node_type', 'type')
      ->fields('type', array('type', 'name'))
      ->execute()
      ->fetchAll();
  foreach ($tree as $mail => $pool) {
    foreach ($pool as $pool_key => $pool_value) {
      foreach ($node_types as $name) {
        if ($pool_key == $name->type) {
          $tree[$mail][$name->name] = $pool_value;
          unset($tree[$mail][$name->type]);
        }
      }
    }
  }
  //dpm(get_defined_vars());
  return $tree;
}

/**
 * build the mailing pool
 * @return array
 */
function _get_pool() {
  $pool = array();
  $pl = db_select('easysignup_nodes', 'ep')
      ->fields('ep', array('nid', 'type'))
      ->condition('in_pool', 1)
      ->execute();
  while ($f = $pl->fetchAssoc()) {
    if (isset($f)) {
      $pool[$f['nid']] = $f['type'];
    }
  }//get_defined_vars();
  return $pool;
}