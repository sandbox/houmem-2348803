The easy signup module allows users (registered and not registered) to sign up for content types and to be notified when content is added or updated.
The admin has the ability to choose content types that will be exposed to the visitors.
In the notification email, the are two links to unsubscribe or to update content types that the current user is signed up to.

Configuration
-------------
-Go to admin/structure/easysignup and activate some content types to be exposed
 to the end user.
-Go to admin/structure/block and assign the "easysignup" Block to a region


-The subscribers list is under admin/structure/easysignup/subscribers_list