<?php

/**
 * Configure easysignup settings.
 */
function easysignup_admin_settings() {
  $form['admin_config'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom sender'),
    '#default_value' => variable_get('easysignup_admin', ''),
    '#description' => t('Enter a name to be used as sender in the headers when sending email with this module. Default value is the name'), //new_string
  );

  $form['email_config'] = array(
    '#type' => 'textfield',
    '#title' => t('email'),
    '#default_value' => variable_get('easysignup_mail', ''),
    '#description' => t('Enter a valid email address to be used in the headers when sending email with this module. Default value is the site email'), //new_string
  );

  $content_types = _get_content_types();
  if (count($content_types) > 0) {
    $checked = variable_get('easysignup_active', array());
    $form['content_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content types'),
      '#description' => t('All available content types.'),
      '#options' => $content_types,
      '#default_value' => drupal_map_assoc(array_keys($checked)),
    );
  }
  else {
    $form['no_content_type'] = array(
      '#type' => 'markup',
      '#markup' => t('No content types available')
    );
  }
  $form['#submit'][] = 'easysignup_custom_settings';
  //dpm(get_defined_vars());
  return system_settings_form($form, FALSE);
}

/**
 * callback for easysignup_admin_settings
 * @param type $form
 * @param type $form_state
 */
function easysignup_custom_settings($form, $form_state) {
  $mail = $form_state['values']['email_config'];
  $admin = $form_state['values']['admin_config'];
  //if email field is not empty
  if ($mail !== '' && !valid_email_address($mail)) {
    $mail = $form_state['values']['email_config'];
    form_set_error('email_config', t('The email address is not valid!')); //new_string
  }
  else {
    $content_types = _get_content_types();
    if (isset($form_state['values']['content_types'])) {
      $items = $form_state['values']['content_types'];
      foreach ($items as $type => $name) {
        if ($name !== 0) {
          foreach ($content_types as $t => $v) {
            if ($type == $t) {
              $items[$type] = $v;
            }
          }
        }
        else {
          unset($items[$type]);
        }
      }
    }
    variable_set('easysignup_active', $items);
    variable_set('easysignup_mail', $mail);
    variable_set('easysignup_admin', $admin);
  }//dpm(get_defined_vars());
}
