<?php

module_load_include('inc', 'easysignup', 'callbacks');

/**
 * implements hook_init().
 */
function easysignup_init() {
  //$tree = _mailing_list();
  #if (!empty($tree)) {
  # _send_mass($tree);
  #}
  #dpm(get_defined_vars());
}

/**
 * implements hook_permission().
 */
function easysignup_permission() {
  return array(
    'administer easysignup' => array(
      'title' => t('Administer Easy signup'),
      'description' => t('Administer Easy signup.'),
    ),
  );
}

/**
 * implements hook_help().
 */
function easysignup_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/modules#description':
      $output .= t('The Easy signup module generates a block so that users can signup and be notified for new content');
      break;
  }
  return $output;
}

/**
 * implements hook_menu().
 */
function easysignup_menu() {
  $items['admin/structure/easysignup'] = array(
    'title' => 'Easy signup',
    'description' => t('Configure Easy signup'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('easysignup_admin_settings'),
    'access arguments' => array('administer easysignup'),
    'file' => 'easysignup.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['confirm_signup'] = array(
    'title' => 'Your registration has been successful.',
    'description' => t('your subscription is confirmed'),
    'page callback' => '_confirmation',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['manage'] = array(
    'title' => 'Easy signup manage subscription',
    'description' => t('Managing subscription'),
    'page callback' => '_manage',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['unsubsribe'] = array(
    'title' => 'Easy signup unsubscribe',
    'description' => t('Unsubsribe'),
    'page callback' => '_unsubsribe',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['admin/structure/easysignup/subscribers_list'] = array(
    'title' => 'Subscribers list',
    'description' => t('Subscribers list: a list of current suscribers'),
    'page callback' => '_subscribers_list',
    'access callback' => 'user_access',
    'access arguments' => array('administer easysignup'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * generates the block form
 * @param type $form
 * @param type $form_state
 * @return string
 */
function easysignup_form($form, &$form_state) {
  $active = variable_get('easysignup_active', array());
  $form['presentation'] = array(
    '#type' => 'markup',
    '#markup' => t('Be top-informed! Receive the latest articles automatically  by email.'),
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('email'),
    '#required' => TRUE
  );
  $form['choose'] = array(
    '#type' => 'fieldset',
    '#title' => t('Your interests'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['choose']['description'] = array(
    '#type' => 'markup',
    '#markup' => t('Select your individual interests from the following areas.'),
  );
  $form['choose']['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Categories'),
    '#options' => drupal_map_assoc($active),
    '#required' => TRUE
  );
  $form['spm'] = array(//anti spam
    '#type' => 'textfield',
    '#title' => t('Name'),
  );
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'easysignup') . '/easysignup.css',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
    '#value' => t('Go'),
  );
  return $form;
}

/**
 * validate the block form
 * @param type $form
 * @param type $form_state
 */
function easysignup_form_validate($form, &$form_state) {
  //if security field is empty
  if ($form_state['values']['spm'] === '') {
    $mail = $form_state['values']['email'];
    if (!valid_email_address($mail)) {
      form_set_error('["email"]', t('Please enter a valid email address!'));
    }
  }
  else {
    form_set_error('["spm"]', t('No submission.'));
  }//dpm(get_defined_vars());
}

/**
 * callback function to do operations when submitting the block form
 * @global type $base_url
 * @param type $form
 * @param type $form_state
 */
function easysignup_form_submit($form, &$form_state) {
  global $base_url;
  $mail = $form_state['values']['email'];
  $hash = _generate_hash();
  $link = $base_url . '/confirm_signup?c=' . $hash;
  $verif = _subscription_verif($mail);
  if ($verif == FALSE) {
    $types = array();
    foreach ($form_state['values']['content_types'] as $b) {
      if ($b !== 0) {
        $types[] = $b;
      }
    }
    $bundles = serialize($types);
    _record_user($mail, $hash, $bundles);
    _send_confirmation_mail($mail, $link);
    drupal_set_message(t('You will receive an email with a confirmation link. By clicking on the link, the registration process will be completed.'));
  }
  else {
    drupal_set_message(t('This email address is already used. Please use another email address.'), 'error');
  }
}

/**
 * implements hook_block_info()
 */
function easysignup_block_info() {
  $blocks['easysignup'] = array(
    'info' => 'easysignup'
  );
  return $blocks;
}

/**
 * implements hook_block_view()
 */
function easysignup_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'easysignup':
      $active_nodes = variable_get('easysignup_active', array());
      $block['subject'] = '';
      if (count($active_nodes) > 0) {
        $block['content'] = drupal_get_form('easysignup_form');
      }
      else {
        $block['content'] = t('No content type available');
      }
      break;
  }//dpm(get_defined_vars());
  return $block;
}

/**
 * Implements hook_node_type_delete();
 */
function easysignup_node_type_delete($info) {
  $active_nodes = variable_get('easysignup_active', array());
  if (!empty($active_nodes)) {
    foreach ($active_nodes as $machine => $name) {
      if ($machine == $info->type) {
        unset($active_nodes['machine']);
        variable_set('easysignup_active', $active_nodes);
      }
    }
  }
  //delete nodes from the node table
  db_delete('node')->condition('type', $info->type)->execute();
  //delete nids from pool
  db_delete('easysignup_nodes')->condition('type', $info->type)->execute();
//delete nids from bundles
  $tabs = db_select('easysignup_users', 'es')
      ->fields('es', array('bundles', 'email'))
      ->execute()
      ->fetchAll();
  foreach ($tabs as $tab) {
    $list = unserialize($tab->bundles);
    foreach ($list as $item => $val) {
      if ($info->type == $val) {
        unset($list[$item]);
        db_update('easysignup_users')
            ->fields(array('bundles' => serialize($list)))
            ->condition('email', $tab->email)
            ->execute();
      }
    }
  }//dpm(get_defined_vars());
}

/**
 * add a checkbox to the node_edit_form, if checked, the current node will
 * be aded to the mailing pool
 */
function easysignup_form_node_form_alter(&$form, $form_state) {
  $active = variable_get('easysignup_active', array());
  $item = $form['type']['#value'];
  if (array_key_exists($item, $active)) {
    $nid = isset($form['#node']->nid) ? $form['#node']->nid : 0;
    $form['pool'] = array(
      '#type' => 'checkbox',
      '#title' => t('Notification'),
      '#description' => t('add current node to the notification pool'),
      '#default_value' => _node_status($nid),
    );
  }//dpm(get_defined_vars());
}

/**
 * implements hook_node_update().
 * when updating a node, update the mailing pool
 */
function easysignup_node_update($node) {
  $active = variable_get('easysignup_active', array());
  if (array_key_exists($node->type, $active)) {
    if (isset($node->pool) && $node->pool == 1) {
      //if current node isn't in the pool table, store it
      $stored_in_pool = db_select('easysignup_nodes', 'pool')
          ->fields('pool', array('nid'))
          ->condition('nid', $node->nid)
          ->execute()
          ->fetchField();
      if ($stored_in_pool == FALSE) {
        db_insert('easysignup_nodes')
            ->fields(array(
              'nid' => $node->nid,
              'type' => $node->type,
              'in_pool' => 1
            ))
            ->execute();
      }
      //if node is in pool, set it to 1
      db_update('easysignup_nodes')
          ->fields(array('in_pool' => 1))
          ->condition('nid', $node->nid)
          ->execute();
    }
    if (isset($node->pool) && $node->pool == 0) {
      db_delete('easysignup_nodes')->condition('nid', $node->nid)->execute();
    }//dpm(get_defined_vars());
  }
}

/**
 * implements hook_node_insert().
 * when creating a node, update the mailing pool
 */
function easysignup_node_insert($node) {
  if (isset($node->pool) && $node->pool == 1) {
    db_insert('easysignup_nodes')
        ->fields(array(
          'nid' => $node->nid,
          'type' => $node->type,
          'in_pool' => 1
        ))
        ->execute();
  }//dpm(get_defined_vars());
}

/**
 * implements hook_node_delete().
 * when delating a node, update the mailing pool
 */
function easysignup_node_delete($node) {
  db_delete('easysignup_nodes')->condition('nid', $node->nid)->execute();
}

/**
 * implements hook_cron().
 */
function easysignup_cron() {
  $tree = _mailing_list();
  if (!empty($tree)) {
    _send_mass($tree);
  }//dpm(get_defined_vars());
}

/**
 * @todo update things after disabling content types under admin/structure/easysignup
 * @todo update node type name after renaming a node type
 */